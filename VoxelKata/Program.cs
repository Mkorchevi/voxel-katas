﻿using System;

namespace VoxelKata
{
    class Program
    {
        static void Main(string[] args)
        {
            Board Board = new Board();
            
            Obstacle Snake1 = new Snake("snake1", 5);
            Obstacle Snake2 = new Snake("snake2", 24);
            Obstacle Snake3 = new Snake("snake3", 10);
            Obstacle Snake4 = new Snake("snake4", 18);
            Obstacle Snake5 = new Snake("snake5", 59);
            Obstacle Snake6 = new Snake("snake6", 52);
            Obstacle Snake7 = new Snake("snake7", 67);
            Obstacle Snake8 = new Snake("snake8", 87);
            Obstacle Snake9 = new Snake("snake9", 74);
            Obstacle Snake10 = new Snake("snake10", 79);

            Obstacle Ladders1 = new Ladders("Ladder1", 37);
            Obstacle Ladders2 = new Ladders("Ladder2", 13);
            Obstacle Ladders3 = new Ladders("Ladder3", 30);
            Obstacle Ladders4 = new Ladders("Ladder4", 25);
            Obstacle Ladders5 = new Ladders("Ladder5", 41);
            Obstacle Ladders6 = new Ladders("Ladder6", 83);
            Obstacle Ladders7 = new Ladders("Ladder7", 43);
            Obstacle Ladders8 = new Ladders("Ladder8", 66);
            Obstacle Ladders9 = new Ladders("Ladder9", 90);
            Obstacle Ladders10 = new Ladders("Ladder10", 97);
            Obstacle Ladders11 = new Ladders("Ladder11", 93);

            Board.Boxes[15] = Snake1;
            Board.Boxes[45] = Snake2;
            Board.Boxes[48] = Snake3;
            Board.Boxes[61] = Snake4;
            Board.Boxes[63] = Snake5;
            Board.Boxes[73] = Snake6;
            Board.Boxes[88] = Snake7;
            Board.Boxes[91] = Snake8;
            Board.Boxes[94] = Snake9;
            Board.Boxes[98] = Snake10;

            Board.Boxes[1] = Ladders1;
            Board.Boxes[6] = Ladders2;
            Board.Boxes[7] = Ladders3;
            Board.Boxes[14] = Ladders4;
            Board.Boxes[20] = Ladders5;
            Board.Boxes[27] = Ladders6;
            Board.Boxes[35] = Ladders7;
            Board.Boxes[50] = Ladders8;
            Board.Boxes[70] = Ladders9;
            Board.Boxes[77] = Ladders10;
            Board.Boxes[86] = Ladders11;

            Console.WriteLine("Let's start the game");
            Console.WriteLine("Player 1 enter your name: ");
            string name1 = Console.ReadLine();
            Player Player1 = new Player(name1);
            Console.WriteLine("Player 2 enter your name: ");
            string name2 = Console.ReadLine();
            Player Player2 = new Player(name2);
            Dice dice = new Dice();

            while (true)
            {
                Player1.MovePlayer(dice);
                Console.WriteLine(Player1.Name + " is now in the position " + Player1.Place);

                if (Board.Boxes[Player1.Place]!= null)
                {
                    Board.Boxes[Player1.Place].Message();
                    Player1.Place = Board.Boxes[Player1.Place].Destiny;
                    Console.WriteLine(Player1.Name + " is now in the position " + Player1.Place);

                }
                if(Player1.Place == 99)
                {
                    Console.WriteLine(Player1.Name + " you have won! you have lost  " + Player2.Name);
                    break;
                }

                Player2.MovePlayer(dice);
                Console.WriteLine(Player2.Name + " is now in the position " + Player2.Place);

                if (Board.Boxes[Player2.Place] != null)
                {
                    Board.Boxes[Player2.Place].Message();
                    Player2.Place = Board.Boxes[Player2.Place].Destiny;
                    Console.WriteLine(Player2.Name + " is now in the position " + Player2.Place);

                }
                if (Player2.Place == 99)
                {
                    Console.WriteLine(Player2.Name + " you have won! you have lost " + Player1.Name);
                    break;
                }


            }

        }
    }
}
