﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelKata
{
    public class Player
    {
        //propiedades 
        public string Name { get; set; }
        public int Place { get; set; }
 
        //constructor
        public Player(string Name)
        {
            this.Name = Name;
            this.Place = 0;
            
        }

        //método
        public void MovePlayer(Dice dice) 
        {
            int newPosition = Place + dice.Roll();
            if (newPosition <= 99)
            {
                Place = newPosition;
            }
        }
       
    }
}
